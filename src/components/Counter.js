import { useDispatch } from "react-redux";
import { updateCounterNumber } from "../features/counter/counterSlice"

const Counter = ({value, index}) => {

    const dispatch = useDispatch()


    const handleIncrease = () => {
        dispatch(updateCounterNumber({index:index,updatedValue:value+1}))
    }

    const handleDecrease = () => {
        dispatch(updateCounterNumber({index:index,updatedValue:value-1}))
    }

    return (
        <div>
            <button onClick={handleIncrease}>+</button>
            <span>{value}</span>
            <button onClick={handleDecrease}>-</button>
        </div>
    )
}

export default Counter
