import { useSelector } from "react-redux";

const CounterGroupSum = () => {
    const counterList = useSelector(state => state.counter.counterList)
    const sum = counterList.reduce((count, number) => count + number ,0)
    return (
        <div>
            sum: {sum}
        </div>
    )
}
export default CounterGroupSum