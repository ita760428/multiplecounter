import { useSelector,useDispatch } from "react-redux";

import { updateCounterSize } from "../features/counter/counterSlice"

const CounterSizeGenerator = () => {
    const counterList = useSelector(state => state.counter.counterList)
    const dispatch = useDispatch()
    const size = counterList.length
    const handleChange = (event) => {
        const currentSize = Number(event.target.value);
        dispatch(updateCounterSize(currentSize))
    }

    return (
        <div>
            size: <input onChange={handleChange} type="number" min={0} value={size} />
        </div>
    )
}

export default CounterSizeGenerator;