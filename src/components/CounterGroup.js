import { useSelector } from "react-redux";
import Counter from './Counter';

const CounterGroup = () => {
    const counterList = useSelector(state => state.counter.counterList)

    return (
        <div>
            {
                counterList.map((value, index) => (
                    <Counter key={index} value={value} index={index} ></Counter>
                ))
            }
        </div>
    )
}

export default CounterGroup;