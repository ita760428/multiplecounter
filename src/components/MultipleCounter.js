import CounterGroup from "./CounterGroup"
import CounterSizeGenerator from "./CounterSizeGenerator"
import CounterGroupSum from "./CounterGroupSum";

const MultipleCounter = () => {
    return (
        <div>
            <CounterSizeGenerator></CounterSizeGenerator>
            <CounterGroupSum></CounterGroupSum>
            <CounterGroup></CounterGroup>
        </div>
    )
}

export default MultipleCounter