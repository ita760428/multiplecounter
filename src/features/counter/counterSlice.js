import { createSlice } from '@reduxjs/toolkit'

export const counterSlice = createSlice({
  name: 'counter',
  initialState: {
    counterList: [1,2,3],
  },
  reducers: {
    incrementByAmount: (state, action) => {
    //   state.value += action.payload
    },

    updateCounterSize: (state, action) => {
        state.counterList = Array(action.payload).fill(0)
    },

    updateCounterNumber: (state, action) => {
        let newCounterList = [...state.counterList]
        newCounterList[action.payload.index] = action.payload.updatedValue
        state.counterList = newCounterList
    },
  },
})

// Action creators are generated for each case reducer function
export const { incrementByAmount ,updateCounterSize,updateCounterNumber} = counterSlice.actions

export default counterSlice.reducer